const Dapp = {
  userAddress: undefined,
  pollFactoryAddress: null,
  
  createNewAccount: function() { //new accounts for patient
    Dapp.web3.personal.newAccount(
      prompt("Please enter your password"),
      function(error, address) {
        if (error) {
          alert(error);
        } else {
          Dapp.setUserAddress(address);
          $(".nav-tabs .nav-link").removeClass("disabled");
        }
      }
    );
  },
  
    inputUserAddress: function() {
    var address = prompt("Please enter address", "0x");
    if (!Dapp.web3.isAddress(address)) {
      alert("Please input a valid address!");
      return;
    }

    Dapp.web3.personal.unlockAccount(
      address,
      prompt("Please enter your password for this address"),
      function(error, result) {
        if (error) {
          alert(error);
        } else {
          Dapp.setUserAddress(address);
          $(".nav-tabs .nav-link").removeClass("disabled");
        }
      }
    );
  },
    setUserAddress: function(address) {
    Dapp.userAddress = address;
    $(".user-address").text(address);
    Dapp.refreshUserBalance();
  },
}