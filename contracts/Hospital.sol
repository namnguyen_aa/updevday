pragma solidity ^0.4.17;
contract Hospital {
    address addr;
    string public name;
    address[] hospitalExaminationRequests;
    
    
    function createExaminationRequest(Patient patient) public returns(ExaminationRequest) {
        
    }
    
    function lookupExaminationRequestHistory(Patient patient) public returns(ExaminationRequest[]) {
        
    }
    
    function finishExaminationStatus(ExaminationRequest examinationRequest) public {
        
    }
    
}

contract Patient {
    address addr;
    string public name;
    address[] patientExaminationRequests;
    
    constructor(string _name) public {
        name = _name;
    }
    
    function payExaminationRequestCost(Hospital hospital) public {
        ExaminationRequest examinationRequest = hospital.createExaminationRequest(this);
        patientExaminationRequests.push()
        
        
    }
    
    function lookupExaminationRequestHistory() public returns(ExaminationRequest[]) {
        
    }
}

contract ExaminationRequest {
    string public name;
    string public result;
    uint public cost;
    string public status;
}